import java.io.IOException;

public class ServerCSocket {

    private ServerPSocket serverPSocket = null;

    public ServerCSocket(int port) throws IOException {
        serverPSocket = new ServerPSocket(port);
    }

    public C_socket accept() throws IOException {
        PSocket p = serverPSocket.accept();
        return new C_socket(p);
    }


}
