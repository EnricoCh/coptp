import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;

public class ClientPSocket {

    /*
     * method connect
     * it creates a PSocket to the specified host, port and returns it
     */
    public PSocket connect(String host, int port) throws IOException {
        Socket socket = new Socket();
        socket.connect(new InetSocketAddress(host, port));
        BufferedOutputStream out;
        BufferedInputStream in;
        out = out = new BufferedOutputStream(socket.getOutputStream());
        in = new BufferedInputStream(socket.getInputStream());
        PSocket ps = new PSocket(socket,out,in);
        return ps;
    }

    /*
     * method staticConnect
     * static version of method connect
     * it creates a PSocket to the specified host, port and returns it
     */
    public static PSocket staticConnect(String host, int port) throws IOException {
        Socket socket = new Socket();
        socket.connect(new InetSocketAddress(host, port));
        BufferedOutputStream out;
        BufferedInputStream in;
        out = out = new BufferedOutputStream(socket.getOutputStream());
        in = new BufferedInputStream(socket.getInputStream());
        PSocket ps = new PSocket(socket,out,in);
        return ps;
    }




}
