import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.nio.charset.Charset;

public class PSocket {

    private Socket clientSocket;
    private BufferedOutputStream out;
    private BufferedInputStream in;

    /*
     *constructor for p_socket
     * do not use directly
     * it is returned by ServerPSocket and ClientPSocket
     */
    protected PSocket (Socket s, BufferedOutputStream out, BufferedInputStream in){
        clientSocket = s;
        this.out = out;
        this.in = in;
    }

    /*
     * method send
     * sends a finite array of byte to the receiver
     *
     */
    public void send(byte[] b) throws IOException {
            String len = Integer.toString(b.length);
            len += '\n';
            out.write(len.getBytes("UTF-8")); //.getBytes(StandardCharsets.UTF_8).toString()
            out.write(b);
            out.flush();
    }


    /*
     * method rcv
     * block until a packet is received and return that packet
     * the packet consist of a finite array of byte
     */
    public byte[] rcv() throws IOException {
            String str_size = "";
            while(true) {
                byte[] buffer = new byte[1];
                in.read(buffer);
                char newchar = (new String(buffer, Charset.forName("UTF-8"))).charAt(0);
                if(newchar == '\n'){
                    break;
                }else if(Character.isDigit(newchar)){
                    str_size += newchar;
                }else{
                    throw new IOException("received a malformed packet: char digit or \\n expected but " + newchar + "received");
                }
            }
            byte[] dati = new byte[Integer.parseInt(str_size)];
            for(int i = 0; i < Integer.parseInt(str_size) ; i++){
                byte[] buffer = new byte[1];
                in.read(buffer);
                dati[i] = buffer[0];
            }
            return dati;
    }


}
