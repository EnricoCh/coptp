import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;


public class C_socket {

    private PSocket p_socket;
    private HashMap<String, ArrayList<byte[]>> map = new HashMap<String,ArrayList<byte[]>>();

    public C_socket(String host, int port) throws IOException {
        this.p_socket = ClientPSocket.staticConnect(host, port);
    }

    public C_socket(PSocket socket){
        this.p_socket = socket;
    }

    public void send(String channel_name, byte[] packet) throws IOException {
        p_socket.send(channel_name.getBytes("UTF-8"));
        p_socket.send(packet);
    }

    public byte[] receive(String channel) throws IOException {
        if(map.get(channel) == null){
            map.put(channel, new ArrayList<>());
        }
        while(map.get(channel).isEmpty()){
            next();
        }
        return map.get(channel).remove(0);
    }


    private void next() throws IOException {
        String channel = new String(p_socket.rcv(), Charset.forName("UTF-8"));
        if(map.get(channel) == null){
            map.put(channel,new ArrayList<byte[]>());
        }
        map.get(channel).add(p_socket.rcv());
    }



}
