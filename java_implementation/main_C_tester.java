import java.io.IOException;
import java.nio.charset.Charset;

public class main_C_tester {

    public static void main(String[] args) throws IOException {
        C_socket cs = new C_socket("127.0.0.1", 9988);
        System.out.println(new String(cs.receive("castoro"), Charset.forName("UTF-8")));
        System.out.println(new String(cs.receive("castoro"), Charset.forName("UTF-8")));
        System.out.println(new String(cs.receive("castoro"), Charset.forName("UTF-8")));
        System.out.println(new String(cs.receive("castoro"), Charset.forName("UTF-8")));
        System.out.println(new String(cs.receive("dodo"), Charset.forName("UTF-8")));
    }

}
