import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;


public class ServerPSocket {

    ServerSocket serverSocket = null;


    /*
     * constructor for ServerPSocket
     * creates a Server Listening on the specified port
     */
    public ServerPSocket(int port) throws IOException {
         serverSocket = new ServerSocket(port);
    }

    /*
     * method accept
     * blocks until a connection request is received and returns a PSocket for that connection
     */
    public PSocket accept() throws IOException {
        Socket socket = serverSocket.accept();
        BufferedOutputStream out;
        BufferedInputStream in;
        out = out = new BufferedOutputStream(socket.getOutputStream()); // new PrintWriter(socket.getOutputStream(), true);
        in = new BufferedInputStream(socket.getInputStream());
        PSocket ps = new PSocket(socket,out,in);
        return ps;
    }


}
