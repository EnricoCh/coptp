channel_oriented_connection

this protocol is meant to simplify prototyping of distributed
application by providing an easy-to-use channel-oriented socket

the main functions will be:

    send(channel_name:String, packet_to_send: byte_array): boolean/void
    receive(channel_name:String): byte_array
    
packets sent to a specific channel will be received from the same channel

planned extensions:

    -dinamically increase-decrease the number of sockets used by the protocol