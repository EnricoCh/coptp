// coptptester project main.go
package main

import (
	"COPTPnet"
	"fmt"
)

func main() {
	fmt.Println("Hello World!")

	go func() {
		server := COPTPnet.NewServerCsocket()
		server.StartServer()
		client := server.Accept()
		client.Send("castoro", []byte("diga 1"))
		client.Send("volpe", []byte("volpe rossa 2"))
		client.Send("castoro", []byte("grooossa diga 3"))
		client.Send("pappagallo", []byte("pappagallo pappagallo 4"))
		client.Send("pappagallo", []byte("pappagallo pappagallo 5 "))
		client.Send("pappagallo", []byte("pappagallo pappagallo 6 "))
		client.Send("pappagallo", []byte("pappagallo pappagallo 7 "))
		client.Send("pappagallo", []byte("pappagallo pappagallo 8"))
		fmt.Println("scoiattolo: ", string(client.Receive("scoiattolo")))
		fmt.Println("scoiattolo: ", string(client.Receive("scoiattolo")))
		fmt.Println("scoiattolo: ", string(client.Receive("scoiattolo")))

	}()
	go func() {
		client := COPTPnet.NewCSocket()
		client.Connect("127.0.0.1:1717")
		fmt.Println("castoro: ", string(client.Receive("castoro")))
		fmt.Println("pappagallo: ", string(client.Receive("pappagallo")))
		fmt.Println("pappagallo: ", string(client.Receive("pappagallo")))
		fmt.Println("pappagallo: ", string(client.Receive("pappagallo")))
		fmt.Println("volpe: ", string(client.Receive("volpe")))
		fmt.Println("pappagallo: ", string(client.Receive("pappagallo")))
		fmt.Println("castoro: ", string(client.Receive("castoro")))
		fmt.Println("pappagallo: ", string(client.Receive("pappagallo")))
		client.Send("scoiattolo", []byte("toc toc"))
		client.Send("scoiattolo", []byte("skk"))
		client.Send("scoiattolo", []byte("sksk"))
	}()

	fmt.Scanln()

}
