// COPTPnet project COPTPnet.go
package COPTPnet

import (
	"rtptnet"
)

type Csocket struct {
	conn   rtptnet.Psocket
	buffer map[string]([]([]byte))
}

type ServerCsocket struct {
	conn rtptnet.ServerPsocket
}

func NewServerCsocket() ServerCsocket {
	s := ServerCsocket{}
	return s
}

func NewCSocket() Csocket {
	p := Csocket{}
	p.buffer = make(map[string]([]([]byte)))
	return p
}

func (scs *ServerCsocket) StartServer() {
	scs.conn.StartServer()
}

func (scs *ServerCsocket) Accept() Csocket {
	conn := scs.conn.Accept()
	client := NewCSocket()
	client.conn = conn
	return client
}

func (cs *Csocket) Connect(ipandport string) {
	cs.conn.Connect(ipandport)
}

func (cs *Csocket) Receive(channel string) []byte {

	buff := cs.buffer[channel]
	var x []byte
	if len(buff) > 0 {
		x, cs.buffer[channel] = buff[0], buff[1:]

	} else {
		cs.rcv()
		return cs.Receive(channel)
	}
	return x
}
func (cs Csocket) Send(channel string, msg []byte) {
	cs.conn.SendMsg([]byte(channel))
	cs.conn.SendMsg(msg)
}

func (cs *Csocket) rcv() {
	channel, _ := cs.conn.Receive()
	msg, _ := cs.conn.Receive()
	cs.buffer[string(channel)] = append(cs.buffer[string(channel)], msg)
}
