const net = require('net');
var p_socket = require('./p_socket');
var events = require('events');
var c_RPTP = require('./Client_RPTP');
var eventEmitter = new events.EventEmitter();

var create_c_socket = function(port, host, func_packet_receiver){
    var emitter = new events.EventEmitter();
    c_RPTP.create_and_connect(port,host,function(conn){
        var p_socket = conn;
        _eventEmitter = new events.EventEmitter();
        var next = function (packet){
            var channel = packet;
            console.log("channel: " + channel);
            p_socket.on(function(packet){
                var msg = packet;
                console.log("channel " + channel + " message: " + msg);
                
                emitter.emit(channel, packet);
                p_socket.on(next);
            });
            
        };
        p_socket.on(next);

    });
    func_packet_receiver(emitter);
    return emitter;
}

create_c_socket(9988,"127.0.0.1",function (emitter) {
    emitter.on("castoro",function(data){
        console.log("-------------HO RICEVUTO UN CASTORO " + data);
    });
    emitter.on("dodo",function(data){
        console.log("-------------HO RICEVUTO UN DODO " + data);
    });
});


/*
var channel = class {

    
    constructor(port, host, func_packet_receiver){
        c_RPTP.create_and_connect(port,host,function(conn){
            var p_socket = conn;
            this._p_socket = p_socket;
            _eventEmitter = new events.EventEmitter();
            var next = function (packet){
                var channel = packet;
                console.log("channel: " + channel);
                p_socket.on(function(packet){
                    var msg = packet;
                    console.log("channel " + channel + " message: " + msg);
                    func_packet_receiver(packet);
                    p_socket.on(next);
                });
                
            };
            p_socket.on(next);
    
        });
    }

    send(channel, packet){
        this.packet.send(channel);
        this.p_socket.send(packet);
    }



}


*/






/*
exports.channel = class channel{




    constructor(conn){
        this.p_socket = conn;
        this._eventEmitter = new events.EventEmitter();
        var _this = this;

        var next = function (packet){
            var channel = packet;
            console.log("channel: " + channel);
            _this.p_socket.on(function(packet){
                var msg = packet;
                console.log("channel " + channel + " message: " + msg);
                _this.p_socket.on(next);
            });
            
        };

        _this.p_socket.on(next);
    }
    get eventEmitter() {
        return this._eventEmitter;
    }




}
*/

