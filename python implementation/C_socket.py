import Client_RPTP
#implementazione alternativa, questa non è completa e non ha senso usarla

class c_socket :
    dict = {}

    def __init__(self):
        pass

    def connect(self,_host,_port):
        self.tunnel = Client_RPTP.client_p_socket.create(host=_host, port=_port)

    def rcv_packet(self):
        channel = self.tunnel.rcv_packet()
        data = self.tunnel.rcv_packet()
        if channel not in self.dict:
            self.dict[channel] = []
        self.dict[channel].append(data)

    def rcv(self, _channel):
        if _channel not in self.dict:
            self.dict[_channel] = []
        while len(self.dict[_channel]) == 0:
            self.rcv_packet()
        return self.dict[_channel].popleft()
        pass


c = c_socket()
c.connect("127.0.0.1", 9988)
print(c.rcv("castoro"))
print(c.rcv("dodo"))
