import Client_RPTP
import Server_RPTP_Debug

class ChannelSocket :

    dict = {}

    def __init__(self,packetsocket):
        self.packetsocket = packetsocket

    def receive(self,channel):

        if channel not in self.dict:
            self.dict[channel] = []
        while(len(self.dict[channel]) <= 0):
            channel_mex = self.packetsocket.rcv_packet()
            channel_text = channel_mex.decode('utf-8')
            data_mex = self.packetsocket.rcv_packet()
            #print ("channel text", channel_text)
            #print("data mex",data_mex.decode('utf-8'))
            print("channel text", channel_text,"data mex",data_mex.decode('utf-8'))
            if channel_text not in self.dict:
                self.dict[channel_text] = []
            self.dict[channel_text].append(data_mex)
        return self.dict[channel].pop(0)

        pass

    def send(self,channel,packet):
        if channel not in self.dict:
            self.dict[channel] = []
        self.packetsocket.snd_packet(channel.encode('utf-8'))
        self.packetsocket.snd_packet(packet)


class ChannelSever:
    HOST = '127.0.0.1'
    PORT = 80

    def __init__(self, *, host, port):
        self.HOST = host
        self.PORT = port
        self.RPTPserver = Server_RPTP_Debug.server_p_socket(host=host, port=port)

    def accept(self):
        return  ChannelSocket(self.RPTPserver.accept())


def createChannelSocket (*, host, port) :
    return ChannelSocket(Client_RPTP.client_p_socket.create(host=host,port=port))

#channelserver = ChannelSever(host="127.0.0.1",port=9988)
#channelCONN = channelserver.accept()
#channelCONN.send("dodo","bibendu".encode("utf-8"))


#channel = createChannelSocket(host="127.0.0.1",port=9988)
#("channel castoro", channel.receive("castoro").decode('utf-8'))
#print("channel castoro", channel.receive("castoro").decode('utf-8'))
#print("channel castoro", channel.receive("castoro").decode('utf-8'))
#print("channel castoro", channel.receive("castoro").decode('utf-8'))





